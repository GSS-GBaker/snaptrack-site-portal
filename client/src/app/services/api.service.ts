/**
 * Created by gbaker on 11/17/16.
 */

import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class GSSApiService {

    private _headers = new Headers();

    constructor(private _http: Http ) {

    }

    //
    // PRIVATE
    //
    private _getToken() {

        var token = window.localStorage.getItem('token');

        if(token !== undefined && token !== null) {

            token = JSON.parse(token);
        }

        if(token.token !== undefined) {

            return token.token;
        }
        else {

            return '';
        }
    }

    private _getGetPredicate() {

        var pred = 'token=' + this._getToken();
        return pred;
    }

    //
    // PUBLIC
    //
    login(username: string, pwd: string) {

        return this._http.post('http://localhost:8080/auth/login', { username: username, password: pwd })
            .toPromise();
    }

    getUsers() {

        var url = 'http://localhost:8080/api/admin/users?';

        url = url + this._getGetPredicate();

        return this._http.get(url).toPromise();
    }
}
